package l9.bc;

import java.util.Scanner;

import net.datastructures.Map;
import net.datastructures.UnsortedTableMap;

/* Map Business Case */
public class WordCount {
	public static void main(String[] args) {
		String text = 
				"A map is an abstract data type designed to efficiently store and retrieve values based\r\n" + 
				"upon a uniquely identifying search key for each. Specifically, a map stores keyvalue\r\n" + 
				"pairs (k,v), which we call entries, where k is the key and v is its corresponding\r\n" + 
				"value. Keys are required to be unique, so that the association of keys to values\r\n" + 
				"defines a mapping. Figure 10.1 provides a conceptual illustration of a map using\r\n" + 
				"the file-cabinet metaphor. For a more modern metaphor, think about the web as\r\n" + 
				"being a map whose entries are the web pages. The key of a page is its URL (e.g.,\r\n" + 
				"http://datastructures.net/) and its value is the page content.";

		String keyboardText = 
				// getKeyboardText(); // CTRL + Z
				text;
		
		System.out.println(keyboardText);	
		
		Map<String, Integer> wordFrequences = wordFrequences(keyboardText);
		String word = maxWordCount(keyboardText);
		
		System.out.println("most frequent word: " + word + " -> " + wordFrequences.get(word));
	}
	
	public static String maxWordCount(String text) {
		Map<String, Integer> wordFrequences = wordFrequences(text);
		int max = 0;
		String mostFrequentWord = null;
		for (String word: wordFrequences.keySet()) {
			System.out.println("Word: " + word + ": " + wordFrequences.get(word));
			if (wordFrequences.get(word) > max) {
				max = wordFrequences.get(word);
				mostFrequentWord = word;
			}
		}
		return mostFrequentWord;
	}
	
	public static Map<String, Integer> wordFrequences(String text){
		String[] words = text.split("\\W+");
		Map<String, Integer> wordFrequences = new UnsortedTableMap<>();
		for(String word: words) {
			if (wordFrequences.get(word) == null)
				wordFrequences.put(word, 1);
			else 
				wordFrequences.put(word, wordFrequences.get(word)+1);
		}
		return wordFrequences;
	}
	
	public static String getKeyboardText() {
		String keyboardText = "";
		Scanner scanner = new Scanner(System.in);
		String kLine;
		while (scanner.hasNextLine()) {
		    kLine = scanner.nextLine();
		    if (kLine.isEmpty()) {
		        break;
		    }
		    keyboardText += kLine + "\n";
		}
		scanner.close();
		return keyboardText;
	}
}