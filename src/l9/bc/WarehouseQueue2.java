package l9.bc;

import net.datastructures.ArrayStack;
import net.datastructures.LinkedQueue;
import net.datastructures.Stack;

public class WarehouseQueue2<E> {
	// WarehouseEntry
	public static class WarehouseEntry<E>{
		private E entry;
		private Double amount;
		public E getEntry() {
			return entry;
		}
		public Double getAmount() {
			return amount;
		}
		public WarehouseEntry(E entry, Double amount) {
			this.entry = entry;
			this.amount = amount;
		}
		@Override
		public String toString() {
			return "[WEntry=" + entry + ", amount=" + amount + "]";
		}
	}
	//
//	private Double threshold = 0.0;
	private Double fillingLevel = 0.0;
	private Double warehouseMaxCapacity = 0.0;
	private LinkedQueue<WarehouseEntry<E>> wQueue = new LinkedQueue<>();
	//
	public WarehouseQueue2(
//			Double threshold, 
			Double warehouseMaxCapacity) {
//		this.threshold = threshold;
		this.warehouseMaxCapacity = warehouseMaxCapacity;
	}

	public Stack<E> addIntoWarehouse(E entry, Double amount){
		System.out.println("... adding:" + entry + " with: " + amount);
		// check if there is enough room even after releasing some older items
//		if (this.fillingLevel + amount > this.warehouseMaxCapacity &&
//				this.threshold + amount > this.warehouseMaxCapacity) 
		if (amount > this.warehouseMaxCapacity)
			throw new RuntimeException("Not enough space in warehouse!");
		// firstly remove some items to make enough room for the new entry
		Stack<E> releasedItems = null;
		if (this.fillingLevel + amount > this.warehouseMaxCapacity)
			releasedItems = releaseItemsFromWarehouse(amount);
		
		WarehouseEntry<E> newEntry = new WarehouseEntry<E>(entry, amount);
		// add the new entry
		this.wQueue.enqueue(newEntry);
		// increase warehouse filling level
		this.fillingLevel += newEntry.getAmount();
		return releasedItems;
	}
	
	private Stack<E> releaseItemsFromWarehouse(Double threshold) {
		Stack<E> releasedItems = new ArrayStack<E>();
		WarehouseEntry<E> releasedEntry;
		// release item until warehouse filling level fall under warehouse threshold
		while((this.warehouseMaxCapacity - this.fillingLevel) < threshold) {
			// release item under FIFO rule
			releasedEntry = this.wQueue.dequeue();
			// increase warehouse filling level
			this.fillingLevel -= releasedEntry.getAmount();
			releasedItems.push(releasedEntry.getEntry());
		}
		return releasedItems;
	}

	@Override
	public String toString() {
		return "WarehouseQueue [" + ", fillingLevel=" + fillingLevel + ", warehouseMaxCapacity="
				+ warehouseMaxCapacity + ", wQueue=" + wQueue + "]";
	}

	/*
	 * 
	 */
	public static void main(String[] args) {
		WarehouseQueue2<String> warehouseQueue = new WarehouseQueue2<String>(100.0);
		Stack<String> releasedItems;
		//
		releasedItems = warehouseQueue.addIntoWarehouse("Item_1", 20.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_2", 40.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_3", 30.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_4", 20.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_5", 15.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_6", 10.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_7", 50.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
	}
}
