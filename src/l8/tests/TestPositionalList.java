package l8.tests;

import java.util.Iterator;

import l8.bc.PositionalListScoreboard;

import dsaj.arrays.GameEntry;
import net.datastructures.LinkedPositionalList;
import net.datastructures.Position;
import net.datastructures.PositionalList;

public class TestPositionalList {

	public static void main(String[] args) {
		testPositionalListOps();
//		testPositionalListBoard();
	}
	
	public static void testPositionalListOps() {
		// Define&Create
		PositionalList<String> pList = new LinkedPositionalList<String>();
		
		// Feed
		for (int i = 0; i < 10; i++) {
			pList.addLast("Element_" + i);
		}
		System.out.println("1 ########################");
		System.out.println("pList: " + pList);
		System.out.println("##########################\n");
		
		
		// Manipulate&Process
		Position<String> p0 = pList.first();
		System.out.println("2 ########################");
		System.out.println("p0: " + p0.getElement());
		System.out.println("##########################\n");
		
		
		Position<String> p2 = pList.after(pList.after(p0));
		System.out.println("3 ########################");
		System.out.println("p2: " + p2.getElement());
		System.out.println("##########################\n");
		
	
		pList.remove(p0);
		pList.remove(p2);
		System.out.println("4 ########################");
		System.out.println("pList: " + pList);
		System.out.println("##########################\n");
		
		System.out.println("5 ########################");
		Iterator<String> iterator = pList.iterator();
		while(iterator.hasNext()) {
			System.out.println("pList next element: " + iterator.next());
		}
		System.out.println("##########################\n");	
		
		System.out.println("6 ########################");
		for(String e: pList) {
			System.out.println("pList element: " + e);
		}
		System.out.println("##########################\n");
		
		
		
		System.out.println("7 ########################");
		Iterator<Position<String>> iteratorPozitional = pList.positions().iterator();
		while(iteratorPozitional.hasNext()) {
			System.out.println("pList next element: " + iteratorPozitional.next().getElement());
		}
		System.out.println("##########################\n");
		
		
		System.out.println("7 ########################");
		for(Position<String> pozitie: pList.positions()) {
			System.out.println("pList element: " + pozitie.getElement());
		}
		System.out.println("##########################\n");
	}
	
	
	public static void testPositionalListBoard() {
		// The main method
		PositionalListScoreboard highscores = new PositionalListScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };
		
		
		System.out.println("8 ########################");
		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE);
			highscores.add(gE);
			System.out.println(" Scoreboard: " + highscores);
		}
		System.out.println("##########################\n");
		
		
		
		System.out.println("9 ########################");
		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("##########################\n");
	}

}