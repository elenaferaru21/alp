package l8.ext;

import net.datastructures.ArrayList;
import net.datastructures.LinkedPositionalList;

public class ArrayListExtension<E> extends ArrayList<E>{ 
	// * Single-element-ops ---------------------------------------------
    // addFirst(element) // course
	public void addFirst(E element) {
		// move the last element
		this.add(this.size(), this.get(this.size() - 1));
		System.out.println("_move last: " + this);
		// shift all elements
		for(int i = this.size() - 3; i >= 0; i--) {
			System.out.println("_move: " + this.get(i) + " to: " + (i+1) );
			this.set(i+1, this.get(i));
		}
		// add element 
		this.set(0, element);
	}
	
	public void addLast(E element) {
		this.add(this.size(), element);
	}
	
	public void union(ArrayListExtension<E> secondaryList){
		for(E element: secondaryList)
			this.addLast(element);
	}
	
	public static void main(String... args) {
		ArrayListExtension<String> aList = new ArrayListExtension<String>();
		for(int i=0; i < 10 ; i++) {
			aList.add(i, "Element_" + i);
		}
		System.out.println("1 ########################");
		System.out.println("aList: " + aList);
		System.out.println("##########################\n");
		
		
		aList.addFirst("New_element_");
		
		System.out.println("2 ########################");
		System.out.println("aList::addFirst : " + aList);
		System.out.println("##########################\n");
		
		
		
		ArrayListExtension<String> sList = new ArrayListExtension<String>();
		for(int i=10; i < 20 ; i++) {
			sList.add(i-10, "Element_" + i);
		}
		System.out.println("3 ########################");
		System.out.println("sList: " + sList);
		System.out.println("##########################\n");
		
		
		System.out.println("4 ########################");
		aList.union(sList);
		System.out.println("aList.union(sList): " + aList);
		System.out.println("##########################\n");
	}
	
}
